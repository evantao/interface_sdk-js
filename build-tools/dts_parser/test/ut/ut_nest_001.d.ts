/**
 * the ut for parent node has since tag version, and child nodes's version is not less than parent version
 * 
 * @since 7
 */
export class Test {
  /**
   * @since 7
   */
  name: string;
  /**
   * @since 8
   */
  age: number;
}