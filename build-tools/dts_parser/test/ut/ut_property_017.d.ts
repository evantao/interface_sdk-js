/**
 * the ut for property in namespace, the property has permission,
 * and permission value includes two permissions
 */
export namespace test {
  /**
   * @permission ohos.permission.GET_SENSITIVE_PERMISSIONS or ohos.permission.GRANT_SENSITIVE_PERMISSIONS
   */
  const name: string
}