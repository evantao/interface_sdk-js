/**
 * the ut for enum, the enum value is initialized
 */
export enum ErrorCode {
  PERMISSION_DENY = -3,
  ABILITY_NOT_FOUND = 1,
}