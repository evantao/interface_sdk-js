/**
 * the ut for enum, only the first member has initializer
 */
export enum ErrorCode {
  PERMISSION_DENY = 1,
  ABILITY_NOT_FOUND,
}